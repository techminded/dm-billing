package net.dm.billing.component.scheduling;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import net.dm.billing.entity.TariffPlanFinAccount;
import net.dm.billing.entity.TariffPlanWriteOff;
import net.dm.billing.service.TariffPlanFinAccountService;
import net.dm.billing.service.TariffPlanWriteOffService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    TariffPlanFinAccountService tariffPlanFinAccountService;

    @Autowired
    TariffPlanWriteOffService tariffPlanWriteOffService;

    @Scheduled(cron = "0 3 * * * *")
    public void writeOff() {
        LocalDateTime now = LocalDateTime.now();
        List<TariffPlanFinAccount> tariffs = tariffPlanFinAccountService.findActual(Timestamp.valueOf(now));
        for (TariffPlanFinAccount tariff : tariffs) {
            List<TariffPlanWriteOff> writeOff = tariffPlanWriteOffService.fetchActualForTariffPlanAccount(tariff, Timestamp.valueOf(now));
            if (writeOff == null) {
                tariffPlanFinAccountService.writeOff(tariff, now);
            } else {
                logger.info("write off for tariff plan #" + String.valueOf(tariff.getId()) + " already performed");
            }
        }
    }
}