package net.dm.billing.form;


import net.dm.billing.entity.Payment;

public class PaymentPayForm {

    private long id;

    private String username;

    private int amount;

    public PaymentPayForm() {
    }

    public PaymentPayForm(Payment payment) {
        this.setId(payment.getId());
        this.setAmount(payment.getAmount());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
