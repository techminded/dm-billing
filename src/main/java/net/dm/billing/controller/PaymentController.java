package net.dm.billing.controller;

import net.dm.billing.entity.FinAccount;
import net.dm.billing.entity.FinTransaction;
import net.dm.billing.entity.FinTransactionSplit;
import net.dm.billing.entity.Payment;
import net.dm.billing.form.PaymentPayForm;
import net.dm.billing.service.FinAccountService;
import net.dm.billing.service.FinTransactionService;
import net.dm.billing.service.FinTransactionSplitService;
import net.dm.billing.service.PaymentService;
import net.dm.users.entity.User;
import net.dm.users.service.CustomUserDetailsService;

import org.hibernate.criterion.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private CustomUserDetailsService userService;

    @Autowired
    private FinAccountService finAccountService;

    @Autowired
    private FinTransactionService finTransactionService;

    @Autowired
    private FinTransactionSplitService finTransactionSplitService;

    @Autowired
    private PaymentService paymentService;

    @RequestMapping(value = "/panel/payments", method = RequestMethod.GET)
    public String index(Model model) {

        List<FinAccount> accounts = finAccountService.findByUser(userService.getCurrentUser());
        List<Long> accountIds = accounts.stream().map(FinAccount::getId).collect(Collectors.toList());
        Page<FinTransactionSplit> splits = finTransactionSplitService.findByAccountIdIn(accountIds, new PageRequest(0, 20, Sort.Direction.DESC, "id"));

        model.addAttribute("splits", splits);
        return "classpath:/templates/payment/index";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/panel/payments/manage", method = RequestMethod.GET)
    public String manage(Model model) {

        Page<FinTransaction> transactions = finTransactionService.findAll(new PageRequest(0, 20));

        model.addAttribute("transactions", transactions);
        return "classpath:/templates/payment/index";
    }

    @RequestMapping(value = "/panel/pay", method = RequestMethod.GET)
    public String pay(Model model) {

        User user = userService.getCurrentUser();
        Payment payment = paymentService.createPayment(user);
        payment = paymentService.save(payment);

        model.addAttribute("user", user);
        model.addAttribute("payment", payment);
        return "classpath:/templates/payment/pay";
    }

    @RequestMapping(value = "/panel/pay", method = RequestMethod.POST, consumes = "multipart/form-data")
    public String paySubmit(@ModelAttribute PaymentPayForm paymentForm, Model model) {

        Payment payment = paymentService.findOne(paymentForm.getId());
        payment.setAmount(paymentForm.getAmount());
        paymentService.save(payment);

        return "redirect:" + paymentService.buildSubmitUrl(payment);
    }


    @RequestMapping(value = "/payment/result", method = RequestMethod.GET)
    @ResponseBody
    public String result(Model model) {

        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();

        return paymentService.checkPayment(request);
    }

    @RequestMapping(value = "/payment/success", method = RequestMethod.GET)
    public String success(Model model) {

        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();

        String result = paymentService.checkPayment(request);

        userService.renewCurrentUserData(userService.getCurrentUser());
        return "classpath:/templates/payment/success";
    }

    @RequestMapping(value = "/payment/error", method = RequestMethod.GET)
    public String error(HttpServletRequest request, Model model) {

        paymentService.setErrorPayment(request);
        return "classpath:/templates/payment/error";
    }
}
