package net.dm.billing.entity;


public enum PaymentStatus {
    UNPAYED,
    PAYED,
    ERROR
}
