package net.dm.billing.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class TariffPlanFinAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
    private long id;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="tariff_plan_id")
    private TariffPlan tariffPlan;

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="fin_account_id")
    private FinAccount finAccount;

    private Timestamp dateStart;

    private Timestamp dateEnd;

    @Enumerated(EnumType.STRING)
    private TariffPlanFinAccountStatus status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TariffPlan getTariffPlan() {
        return tariffPlan;
    }

    public void setTariffPlan(TariffPlan tariffPlan) {
        this.tariffPlan = tariffPlan;
    }

    public FinAccount getFinAccount() {
        return finAccount;
    }

    public void setFinAccount(FinAccount finAccount) {
        this.finAccount = finAccount;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }

    public TariffPlanFinAccountStatus getStatus() {
        return status;
    }

    public void setStatus(TariffPlanFinAccountStatus status) {
        this.status = status;
    }
}
