package net.dm.billing.entity;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.sql.Timestamp;

@Entity
public class TariffPlanWriteOff {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
    private long id;

    @OneToOne(fetch= FetchType.EAGER)
    private FinTransaction finTransaction;

    @OneToOne(fetch= FetchType.EAGER)
    private TariffPlanFinAccount tariffPlanFinAccount;

    private Timestamp dateWriteOff;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FinTransaction getFinTransaction() {
        return finTransaction;
    }

    public void setFinTransaction(FinTransaction finTransaction) {
        this.finTransaction = finTransaction;
    }

    public TariffPlanFinAccount getTariffPlanFinAccount() {
        return tariffPlanFinAccount;
    }

    public void setTariffPlanFinAccount(TariffPlanFinAccount tariffPlanFinAccount) {
        this.tariffPlanFinAccount = tariffPlanFinAccount;
    }

    public Timestamp getDateWriteOff() {
        return dateWriteOff;
    }

    public void setDateWriteOff(Timestamp dateWriteOff) {
        this.dateWriteOff = dateWriteOff;
    }
}
