package net.dm.billing.entity;


public enum TariffPlanFinAccountStatus {
    STARTED,
    FINISHED,
    STOPPED
}
