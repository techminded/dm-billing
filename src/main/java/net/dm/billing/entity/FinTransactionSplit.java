package net.dm.billing.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.sql.Timestamp;

@Entity
public class FinTransactionSplit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    private FinAccount account;

    private int amount = 0;

    private Timestamp reconcileDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fin_transaction_id")
    private FinTransaction finTransaction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FinAccount getAccount() {
        return account;
    }

    public void setAccount(FinAccount account) {
        this.account = account;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Timestamp getReconcileDate() {
        return reconcileDate;
    }

    public void setReconcileDate(Timestamp reconcileDate) {
        this.reconcileDate = reconcileDate;
    }

    public FinTransaction getFinTransaction() {
        return finTransaction;
    }

    public void setFinTransaction(FinTransaction finTransaction) {
        this.finTransaction = finTransaction;
    }

}
