package net.dm.billing.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dm.users.entity.User;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
    private long id;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    private String invoiceRef;

    private String comment;

    @Lob
    private String commentData;

    private int amount;

    private String provider;

    @Enumerated(EnumType.STRING)
    private PaymentStatus status;

    private Timestamp date;

    @OneToOne(fetch = FetchType.EAGER)
    private Currency currency;

    @OneToOne(fetch = FetchType.EAGER)
    private FinTransaction finTransaction;

    @Lob
    private String data;

    public String[] getCommentDataUnserialized() throws Exception {
        ObjectMapper m = new ObjectMapper();
        String[] commentData = m.readValue(getCommentData(), String[].class);

        return commentData;
    }

    public Map<Integer, String> getCommentDataMap() throws Exception {
        String[] commentData = this.getCommentDataUnserialized();
        Map<Integer, String> commentDataMap = new HashMap<Integer, String>();
        int i = 0;
        for (String commentDataEntry : commentData) {
            commentDataMap.put(i++, commentDataEntry);
        }
        return commentDataMap;
    }

    // generated methods beloa

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getInvoiceRef() {
        return invoiceRef;
    }

    public void setInvoiceRef(String invoiceRef) {
        this.invoiceRef = invoiceRef;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentData() {
        return commentData;
    }

    public void setCommentData(String commentData) {
        this.commentData = commentData;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public FinTransaction getFinTransaction() {
        return finTransaction;
    }

    public void setFinTransaction(FinTransaction finTransaction) {
        this.finTransaction = finTransaction;
    }
}
