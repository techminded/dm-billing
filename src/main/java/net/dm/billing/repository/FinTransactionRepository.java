package net.dm.billing.repository;

import net.dm.billing.entity.FinTransaction;
import net.dm.users.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FinTransactionRepository extends PagingAndSortingRepository<FinTransaction, Long> {

    public List<FinTransaction> findByUser(@Param("user") User user);

}
