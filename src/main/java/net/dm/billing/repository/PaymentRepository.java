package net.dm.billing.repository;

import net.dm.billing.entity.Payment;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface PaymentRepository extends PagingAndSortingRepository<Payment, Long> {

    public Payment findByInvoiceRef(@Param("invoiceRef") String invoiceRef);

}
