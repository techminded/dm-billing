package net.dm.billing.repository;

import net.dm.billing.entity.Payment;
import net.dm.billing.entity.TariffPlanFinAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface TariffPlanFinAccountRepository extends PagingAndSortingRepository<TariffPlanFinAccount, Long> {

    @Query("select ta from TariffPlanFinAccount ta where ta.dateStart < :datetime and ta.dateEnd > :datetime and ta.status = 'STARTED'")
    public List<TariffPlanFinAccount> findActual(@Param("datetime") Timestamp datetime);

}
