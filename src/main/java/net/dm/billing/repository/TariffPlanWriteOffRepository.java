package net.dm.billing.repository;

import net.dm.billing.entity.TariffPlanFinAccount;
import net.dm.billing.entity.TariffPlanWriteOff;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface TariffPlanWriteOffRepository extends PagingAndSortingRepository<TariffPlanWriteOff, Long> {

    @Query("select to from TariffPlanWriteOff to where to.dateWriteOff > :dateStart and to.dateWriteOff < :dateEnd and to.tariffPlanFinAccount = :account")
    public List<TariffPlanWriteOff> fetchActualForTariffPlanAccount(@Param("account") TariffPlanFinAccount account, @Param("dateStart") Date dateStart, @Param("dateEnd") Date dateEnd);

}
