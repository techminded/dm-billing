package net.dm.billing.repository;

import net.dm.billing.entity.FinTransactionSplit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FinTransactionSplitRepository extends PagingAndSortingRepository<FinTransactionSplit, Long> {

    public List<FinTransactionSplit> findByAccountIdIn(@Param("accountIds") List<Long> accountIds);

    public Page<FinTransactionSplit> findByAccountIdIn(@Param("accountIds") List<Long> accountIds, Pageable pageable);

}
