package net.dm.billing.repository;

import net.dm.billing.entity.Currency;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface CurrencyRepository extends PagingAndSortingRepository<Currency, Long> {

    public Currency findFirstByCode(@Param("code") String code);

}
