package net.dm.billing.repository;

import net.dm.billing.entity.FinAccount;
import net.dm.billing.entity.FinAccountType;
import net.dm.users.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FinAccountRepository extends PagingAndSortingRepository<FinAccount, Long> {

    public List<FinAccount> findByUser(@Param("user") User user);

    public FinAccount findFirstByTitle(@Param("title") String title);

    public FinAccount findFirstByUserAndDefaultAccount(@Param("user") User user, @Param("defaultAccount") boolean defaultAccount);

    public FinAccount findFirstByUserAndFinAccountType(@Param("user") User user, @Param("finAccountType") FinAccountType finAccountType);
}
