package net.dm.billing.repository;


import net.dm.billing.entity.TariffPlan;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface TariffPlanRepository extends PagingAndSortingRepository<TariffPlan, Long> {

    public TariffPlan findFirstByName(@Param("name") String name);

}
