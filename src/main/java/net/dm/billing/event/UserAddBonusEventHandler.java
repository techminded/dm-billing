package net.dm.billing.event;

import net.dm.billing.entity.FinAccount;
import net.dm.billing.entity.FinTransaction;
import net.dm.billing.service.FinAccountService;
import net.dm.billing.service.FinTransactionService;
import net.dm.users.entity.User;
import net.dm.users.event.UserAddBonusEvent;
import net.dm.users.event.UserCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class UserAddBonusEventHandler
   implements ApplicationListener<UserAddBonusEvent>{

   @Autowired
   FinAccountService finAccountService;

   @Autowired
   FinTransactionService finTransactionService;

   public void onApplicationEvent(UserAddBonusEvent event) {
      User user = event.getUser();
      int amount = event.getAmount();
      FinAccount userBonusAccount = finAccountService.findBonusAccountForUser(user);
      FinTransaction bonusTransaction = finTransactionService.addBonus(userBonusAccount, amount);
   }

}