package net.dm.billing.service;


import net.dm.billing.entity.FinAccount;
import net.dm.billing.entity.FinTransaction;
import net.dm.billing.entity.FinTransactionSplit;
import net.dm.billing.entity.TariffPlanFinAccount;
import net.dm.billing.entity.TariffPlanWriteOff;
import net.dm.billing.repository.TariffPlanFinAccountRepository;
import net.dm.users.entity.User;
import net.dm.users.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TariffPlanFinAccountService {

    @Autowired
    private TariffPlanFinAccountRepository tariffPlanFinAccountRepository;

    @Autowired
    private FinAccountService finAccountService;

    @Autowired
    private FinTransactionService finTransactionService;

    @Autowired
    private FinTransactionSplitService finTransactionSplitService;

    @Autowired
    private TariffPlanWriteOffService tariffPlanWriteOffService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private CustomUserDetailsService userService;

    public List<TariffPlanFinAccount> findActual(@Param("datetime") Timestamp datetime) {
        return tariffPlanFinAccountRepository.findActual(datetime);
    }

    public void writeOff(TariffPlanFinAccount tariff, LocalDateTime now) {
        Timestamp transactionDatetime = Timestamp.valueOf(now);
        User payer = tariff.getFinAccount().getUser();
        int amount = tariff.getTariffPlan().getPrice();

        FinAccount serviceAccount = tariff.getFinAccount();
        serviceAccount.setBalance(serviceAccount.getBalance() + amount);
        finAccountService.save(serviceAccount);

        FinTransaction transaction = new FinTransaction();
        transaction.setUser(payer);
        transaction.setDescription("message.service.writeoff");
        finTransactionService.save(transaction);

        FinTransactionSplit fromSplit = new FinTransactionSplit();
        fromSplit.setAccount(serviceAccount);
        fromSplit.setAmount(-amount);
        fromSplit.setReconcileDate(transactionDatetime);
        fromSplit.setFinTransaction(transaction);
        finTransactionSplitService.save(fromSplit);

        FinTransactionSplit toSplit = new FinTransactionSplit();

        FinAccount userAccount = finAccountService.findDefaultForUser(payer);
        userAccount.setBalance(userAccount.getBalance() - amount);
        finAccountService.save(userAccount);

        toSplit.setAccount(userAccount);
        toSplit.setAmount(amount);
        toSplit.setReconcileDate(transactionDatetime);
        toSplit.setFinTransaction(transaction);
        finTransactionSplitService.save(toSplit);

        User user = userAccount.getUser();
        user.setBalance(user.getBalance() - amount);
        user.setBalanceTotal(user.getBalance() - amount);
        userService.save(user);

        TariffPlanWriteOff writeOff = new TariffPlanWriteOff();
        writeOff.setFinTransaction(transaction);
        writeOff.setTariffPlanFinAccount(tariff);
        writeOff.setDateWriteOff(transactionDatetime);
        tariffPlanWriteOffService.save(writeOff);
    }

    public TariffPlanFinAccount save(TariffPlanFinAccount tariffPlanFinAccount) {
        return tariffPlanFinAccountRepository.save(tariffPlanFinAccount);
    }

}
