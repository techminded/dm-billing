package net.dm.billing.service;

import net.dm.billing.entity.Currency;
import net.dm.billing.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;

    public Currency findFirstByCode(String code) {
        return currencyRepository.findFirstByCode(code);
    }
}
