package net.dm.billing.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.dm.billing.entity.FinTransaction;
import net.dm.billing.entity.Payment;
import net.dm.billing.entity.PaymentStatus;
import net.dm.billing.repository.PaymentRepository;
import net.dm.util.service.MailService;
import net.hiwidget.service.MessageByLocaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Service
public class PaymentProviderRobokassa implements PaymentProvider {

    private static final Logger logger = LoggerFactory.getLogger(PaymentProviderRobokassa.class);

    // parameters: merchant login, out sum, invoice id, invoice description, signature crc
    public static final String urlPattern = "https://auth.robokassa.ru/Merchant/Index.aspx?MrchLogin=%s&OutSum=%s&InvId=%s&Desc=%s&SignatureValue=%s";

    @Value("${hiwidget.payment.robokassa.login}")
    private String merchantLogin;

    @Value("${hiwidget.payment.robokassa.password1}")
    private String merchantPassword;

    @Value("${hiwidget.payment.robokassa.password2}")
    private String checkPassword;

    @Autowired
    private MailService mailService;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private FinTransactionService finTransactionService;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    private String getSignature(Payment payment) {
        String crcHashString = "";
        try {
            String crcString = String.format("%s:%s:%s:%s",
                    merchantLogin, String.valueOf(payment.getAmount()), payment.getInvoiceRef(), merchantPassword);
            crcHashString = this.md5(crcString);
        } catch (Exception e) {
            logger.error("Error generating payment signature");
        }
        return crcHashString;
    }

    public String md5(String input) throws NoSuchAlgorithmException {
        String result = input;
        if(input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while(result.length() < 32) { //40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }

    public String buildSubmitUrl(Payment payment) {
        String commentEncoded = "";
        try {
            commentEncoded = URLEncoder.encode(messageByLocaleService.getMessage(payment.getComment(), payment.getCommentDataUnserialized()), "UTF-8");
        } catch (Exception e) {
            logger.error("Error encoding comment string");
        }
        return String.format(urlPattern, merchantLogin, payment.getAmount(), payment.getInvoiceRef(), commentEncoded, this.getSignature(payment));
    }


    public String checkPayment(HttpServletRequest request) {

        String outSumm = request.getParameter("OutSum");
        String invoiceRef = request.getParameter("InvId");
        String shpItem = request.getParameter("Shp_item");
        String crc = request.getParameter("SignatureValue").toUpperCase();

        final Payment payment = paymentRepository.findByInvoiceRef(invoiceRef);
        ObjectMapper m = new ObjectMapper();
        ObjectNode data = m.createObjectNode();
        data.put("OutSum", outSumm);
        data.put("Shp_item", shpItem);
        data.put("OutSum", outSumm);
        data.put("invoiceRef", invoiceRef);
        data.put("crc", crc);
        payment.setData(data.toString());

        if (payment.getFinTransaction() == null) {
            FinTransaction transaction = finTransactionService.createForPayment(payment);
            payment.setFinTransaction(transaction);
        }
        payment.setStatus(PaymentStatus.PAYED);
        paymentRepository.save(payment);

        String myCrc = "";
        try {
            myCrc = this.md5(String.format("%s:%s:%s:Shp_item=%s", outSumm, invoiceRef, checkPassword, shpItem));
        } catch (Exception e) {
            logger.error("Error generating crc hash");
        }
        if (! myCrc.equals(crc)) {
            return "bad sign\n";
        } else {
            Map<String, String> renderData = new HashMap<String, String>() {{
                put("invoiceRef", payment.getInvoiceRef());
                put("amount", String.valueOf(payment.getAmount()));
            }};

            mailService.sendPaymentAdminNotification(renderData);
            return String.format("OK%s\n");
        }

    }

    public Payment setErrorPayment(HttpServletRequest request) {
        Payment payment = null;

        String invoiceRef = request.getParameter("InvId");
        if (invoiceRef != null) {

            payment = paymentRepository.findByInvoiceRef(invoiceRef);

            payment.setStatus(PaymentStatus.ERROR);
            paymentRepository.save(payment);

        }
        return payment;
    }
}
