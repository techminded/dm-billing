package net.dm.billing.service;


import com.fasterxml.jackson.databind.ObjectMapper;

import net.dm.billing.entity.Payment;
import net.dm.billing.entity.PaymentStatus;
import net.dm.users.entity.User;
import net.dm.billing.repository.PaymentRepository;
import net.hiwidget.service.MessageByLocaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;


@Service
public class PaymentService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

    public static final String REFERENCE_DATE_FORMAT = "yyMMddHHmm";

    @Value("${hiwidget.payment.default_amount}")
    public int paymentDefaultAmount;

    @Autowired
    private PaymentProviderRobokassa paymentProviderRobokassa;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    @Value("${hiwidget.payment.servicelegalentityname}")
    private String serviceLegalEntityName;

    public Payment createPayment(User user) {
        Payment payment = new Payment();
        OffsetDateTime now = OffsetDateTime.now();
        payment.setInvoiceRef(now.format(DateTimeFormatter.ofPattern((REFERENCE_DATE_FORMAT))));
        payment.setComment("payment.field.comment.default");
        ObjectMapper m = new ObjectMapper();
        try {
            payment.setCommentData(m.writeValueAsString(new String[]{serviceLegalEntityName}));
        } catch (Exception e) {
            logger.error("Error serializing payment comment parameters");
        }
        payment.setAmount(paymentDefaultAmount);
        payment.setStatus(PaymentStatus.UNPAYED);
        payment.setDate(Timestamp.valueOf(LocalDateTime.now()));
        payment.setUser(user);
        payment.setProvider("robokassa");

        return payment;
    }

    public Payment findOne(long id) {
        return paymentRepository.findOne(id);
    }

    public Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }

    public String buildSubmitUrl(Payment payment) {
        return paymentProviderRobokassa.buildSubmitUrl(payment);
    }

    public String checkPayment(HttpServletRequest request) {
        return paymentProviderRobokassa.checkPayment(request);
    }

    public Payment setErrorPayment(HttpServletRequest request) {
        return paymentProviderRobokassa.setErrorPayment(request);
    }

}
