package net.dm.billing.service;


import net.dm.billing.entity.FinAccount;
import net.dm.billing.entity.FinTransaction;
import net.dm.billing.entity.FinTransactionSplit;
import net.dm.billing.entity.Payment;
import net.dm.users.entity.User;
import net.dm.billing.repository.FinAccountRepository;
import net.dm.billing.repository.FinTransactionRepository;
import net.dm.billing.repository.FinTransactionSplitRepository;
import net.hiwidget.service.MessageByLocaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class FinTransactionService {


    @Autowired
    private FinTransactionRepository finTransactionRepository;

    @Autowired
    private FinAccountRepository finAccountRepository;

    @Autowired
    private FinAccountService finAccountService;

    @Autowired
    private FinTransactionSplitRepository finTransactionSplitRepository;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    @Value("${hiwidget.registration.reward}")
    public int registrationReward;

    public FinTransaction save(FinTransaction finTransaction) {
        return finTransactionRepository.save(finTransaction);
    }

    /**
     * create transaction with two splits related, update debit and credict account balances
     * @param payment
     * @return
     */
    public FinTransaction createForPayment(Payment payment) {

        FinTransaction transaction = new FinTransaction();
        transaction.setUser(payment.getUser());
        transaction.setDescription(payment.getComment());
        finTransactionRepository.save(transaction);

        FinTransactionSplit fromSplit = new FinTransactionSplit();
        FinAccount providerAccount = finAccountRepository.findFirstByTitle(payment.getProvider());
        providerAccount.setBalance(providerAccount.getBalance() - payment.getAmount());
        fromSplit.setAccount(providerAccount);
        fromSplit.setAmount(-payment.getAmount());
        fromSplit.setReconcileDate(payment.getDate());
        fromSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(fromSplit);

        FinTransactionSplit toSplit = new FinTransactionSplit();
        User payer = payment.getUser();
        payer.setBalance(payer.getBalance() + payment.getAmount());
        payer.setBalanceTotal(payer.getBalanceTotal() + payment.getAmount());
        FinAccount userAccount = finAccountService.findDefaultForUser(payer);
        userAccount.setBalance(userAccount.getBalance() + payment.getAmount());

        toSplit.setAccount(userAccount);
        toSplit.setAmount(payment.getAmount());
        toSplit.setReconcileDate(payment.getDate());
        toSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(toSplit);

//        Set<FinTransactionSplit> splits = new HashSet<>();
//        splits.add(fromSplit);
//        splits.add(toSplit);
//        transaction.setSplits(splits);

        return transaction;
    }

    public FinTransaction createRegistrationReward(FinAccount userAccount) {
        Timestamp transactionDatetime = Timestamp.valueOf(LocalDateTime.now());
        FinAccount serviceAccount = finAccountRepository.findFirstByTitle(FinAccountService.REWARD_ACCOUNT_NAME);
        serviceAccount.setBalance(serviceAccount.getBalance() - registrationReward);

        FinTransaction transaction = new FinTransaction();
        transaction.setUser(userAccount.getUser());
        transaction.setDescription("message.reward.registration");
        finTransactionRepository.save(transaction);

        FinTransactionSplit fromSplit = new FinTransactionSplit();
        fromSplit.setAccount(serviceAccount);
        fromSplit.setAmount(-registrationReward);
        fromSplit.setReconcileDate(transactionDatetime);
        fromSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(fromSplit);

        FinTransactionSplit toSplit = new FinTransactionSplit();

        userAccount.setBalance(userAccount.getBalance() + registrationReward);

        toSplit.setAccount(userAccount);
        toSplit.setAmount(registrationReward);
        toSplit.setReconcileDate(transactionDatetime);
        toSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(toSplit);

        User user = userAccount.getUser();
        user.setBalanceBonus(user.getBalanceBonus() + registrationReward);
        user.setBalanceTotal(user.getBalanceBonus() + user.getBalance());
        return finTransactionRepository.save(transaction);
    }

    public FinTransaction addBonus(FinAccount userAccount, int amount) {
        Timestamp transactionDatetime = Timestamp.valueOf(LocalDateTime.now());
        FinAccount serviceAccount = finAccountRepository.findFirstByTitle(FinAccountService.REWARD_ACCOUNT_NAME);
        serviceAccount.setBalance(serviceAccount.getBalance() - amount);

        FinTransaction transaction = new FinTransaction();
        transaction.setUser(userAccount.getUser());
        transaction.setDescription("message.bonus.added");
        finTransactionRepository.save(transaction);

        FinTransactionSplit fromSplit = new FinTransactionSplit();
        fromSplit.setAccount(serviceAccount);
        fromSplit.setAmount(-amount);
        fromSplit.setReconcileDate(transactionDatetime);
        fromSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(fromSplit);

        FinTransactionSplit toSplit = new FinTransactionSplit();

        userAccount.setBalance(userAccount.getBalance() + amount);

        toSplit.setAccount(userAccount);
        toSplit.setAmount(amount);
        toSplit.setReconcileDate(transactionDatetime);
        toSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(toSplit);

        User user = userAccount.getUser();
        user.setBalanceBonus(user.getBalanceBonus() + amount);
        user.setBalanceTotal(user.getBalanceBonus() + user.getBalance());
        return finTransactionRepository.save(transaction);
    }


    public FinTransaction createTariffPlanWriteOff(FinAccount userAccount) {
        Timestamp transactionDatetime = Timestamp.valueOf(LocalDateTime.now());
        FinAccount serviceAccount = finAccountRepository.findFirstByTitle(FinAccountService.REWARD_ACCOUNT_NAME);
        serviceAccount.setBalance(serviceAccount.getBalance() - registrationReward);

        FinTransaction transaction = new FinTransaction();
        transaction.setUser(userAccount.getUser());
        transaction.setDescription("message.reward.registration");
        finTransactionRepository.save(transaction);

        FinTransactionSplit fromSplit = new FinTransactionSplit();
        fromSplit.setAccount(serviceAccount);
        fromSplit.setAmount(-registrationReward);
        fromSplit.setReconcileDate(transactionDatetime);
        fromSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(fromSplit);

        FinTransactionSplit toSplit = new FinTransactionSplit();

        userAccount.setBalance(userAccount.getBalance() + registrationReward);

        toSplit.setAccount(userAccount);
        toSplit.setAmount(registrationReward);
        toSplit.setReconcileDate(transactionDatetime);
        toSplit.setFinTransaction(transaction);
        finTransactionSplitRepository.save(toSplit);

        User user = userAccount.getUser();
        user.setBalanceBonus(user.getBalanceBonus() + registrationReward);
        user.setBalanceTotal(user.getBalanceBonus() + user.getBalance());
        return finTransactionRepository.save(transaction);
    }

    public Page<FinTransaction> findAll(Pageable pageable) {
        return finTransactionRepository.findAll(pageable);
    }
}
