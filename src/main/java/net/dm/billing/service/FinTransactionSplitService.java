package net.dm.billing.service;


import net.dm.billing.entity.FinTransactionSplit;
import net.dm.billing.repository.FinTransactionSplitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinTransactionSplitService {

    @Autowired
    private FinTransactionSplitRepository finTransactionSplitRepository;

    public List<FinTransactionSplit> findByAccountIdIn(List<Long> accountIds) {
        return finTransactionSplitRepository.findByAccountIdIn(accountIds);
    }
    public Page<FinTransactionSplit> findByAccountIdIn(List<Long> accountIds, Pageable pageable) {
        return finTransactionSplitRepository.findByAccountIdIn(accountIds, pageable);
    }
    public FinTransactionSplit save(FinTransactionSplit finTransactionSplit) {
        return finTransactionSplitRepository.save(finTransactionSplit);
    }

}
