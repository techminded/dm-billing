package net.dm.billing.service;

import net.dm.billing.entity.TariffPlan;

import net.dm.billing.repository.TariffPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TariffPlanService {

    @Autowired
    private TariffPlanRepository tariffPlanRepository;

    public TariffPlan findFirstByName(String name) {
        return tariffPlanRepository.findFirstByName(name);
    }

}
