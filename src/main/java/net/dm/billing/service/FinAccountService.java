package net.dm.billing.service;


import net.dm.billing.entity.FinAccount;
import net.dm.billing.entity.FinAccountType;
import net.dm.users.entity.User;
import net.dm.billing.repository.FinAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinAccountService {

    public static final String REWARD_ACCOUNT_NAME = "Service Rewards";

    @Autowired
    private FinAccountRepository finAccountRepository;

    @Autowired
    private CurrencyService currencyService;

    public List<FinAccount> findByUser(@Param("user") User user) {
        return finAccountRepository.findByUser(user);
    }

    public FinAccount findFirstByTitle(@Param("title") String title) {
        return finAccountRepository.findFirstByTitle(title);
    }

    public FinAccount findDefaultForUser(@Param("user") User user) {
        return finAccountRepository.findFirstByUserAndDefaultAccount(user, true);
    }
    public FinAccount findBonusAccountForUser(@Param("user") User user) {
        return finAccountRepository.findFirstByUserAndFinAccountType(user, FinAccountType.USER_BONUS);
    }

    public FinAccount save(FinAccount finAccount) {
        return finAccountRepository.save(finAccount);
    }

    public FinAccount createUserDefaultAccount(User user) {
        FinAccount userDefaultFinAccount = new FinAccount();
        userDefaultFinAccount.setBalance(0);
        userDefaultFinAccount.setFinAccountType(FinAccountType.USER);
        userDefaultFinAccount.setDefaultAccount(true);
        userDefaultFinAccount.setTitle("user #" + user.getId() + ", " + user.getEmail() + " default account");
        userDefaultFinAccount.setUser(user);
        userDefaultFinAccount.setCurrency(currencyService.findFirstByCode("RUB"));
        return this.save(userDefaultFinAccount);
    }

    public FinAccount createUserBonusAccount(User user) {
        FinAccount userBonusFinAccount = new FinAccount();
        userBonusFinAccount.setBalance(0);
        userBonusFinAccount.setFinAccountType(FinAccountType.USER_BONUS);
        userBonusFinAccount.setDefaultAccount(false);
        userBonusFinAccount.setTitle("user #" + user.getId() + ", " + user.getEmail() + " bonus account");
        userBonusFinAccount.setUser(user);
        userBonusFinAccount.setCurrency(currencyService.findFirstByCode("RUB"));
        return this.save(userBonusFinAccount);
    }
}
