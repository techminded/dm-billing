package net.dm.billing.service;

import net.dm.billing.entity.TariffPlanFinAccount;
import net.dm.billing.entity.TariffPlanWriteOff;
import net.dm.billing.repository.TariffPlanWriteOffRepository;
import net.dm.billing.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class TariffPlanWriteOffService {

    @Autowired
    private TariffPlanWriteOffRepository tariffPlanWriteOffRepository;

    public List<TariffPlanWriteOff> fetchActualForTariffPlanAccount(TariffPlanFinAccount account, Timestamp datetime) {
        Date dateStart = DateTimeUtil.getStartOfDay(datetime);
        Date dateEnd = DateTimeUtil.getEndOfDay(datetime);
        return tariffPlanWriteOffRepository.fetchActualForTariffPlanAccount(account, dateStart, dateEnd);
    }

    public TariffPlanWriteOff save(TariffPlanWriteOff tariffPlanWriteOff) {
        return tariffPlanWriteOffRepository.save(tariffPlanWriteOff);
    }
}
